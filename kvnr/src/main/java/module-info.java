/* VBU Krankenversichertennummer - Copyright (C) 2022 BKK VBU
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * https://www.gnu.org/licenses/lgpl-3.0.en.html
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/**
 * This module offers an implementation of the German <a href='https://de.wikipedia.org/wiki/Krankenversichertennummer'>Krankenversichertennummer</a> (10-digit invariant part) with full validation.
 *
 * <p>With the service provider interface it's possible to support <a href='https://en.wikipedia.org/wiki/General_Data_Protection_Regulation'>GDPR</a>
 * friendly test {@link tech.vbu.kvnr.Krankenversichertennummer} values.</p>
 *
 * @see tech.vbu.kvnr.spi.TestKrankenversichertennummerValueProvider
 */
module tech.vbu.krankenversichertennummer {
    exports tech.vbu.kvnr;
    exports tech.vbu.kvnr.spi;
    uses tech.vbu.kvnr.spi.TestKrankenversichertennummerValueProvider;
}
