/* VBU Krankenversichertennummer - Copyright (C) 2022 BKK VBU
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * https://www.gnu.org/licenses/lgpl-3.0.en.html
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package tech.vbu.kvnr;

/**
 * When thrown the exception shows a problem with the
 * <a href='https://en.wikipedia.org/wiki/Luhn_algorithm'>Luhn</a> check of the Krankenversichertennummer
 * checksum algorithm.
 */
public class ChecksumException extends KrankenversichertennummerException {

    /** Constructs an {@code ChecksumException} with no detail message. */
    public ChecksumException() {
        super();
    }

    /**
     * Constructs an {@code ChecksumException} with the specified detail message.
     * @param s the detail message.
     */
    public ChecksumException(final String s) {
        super(s);
    }
}
