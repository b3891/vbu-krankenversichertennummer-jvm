/* VBU Krankenversichertennummer - Copyright (C) 2022 BKK VBU
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * https://www.gnu.org/licenses/lgpl-3.0.en.html
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package tech.vbu.kvnr;

import tech.vbu.kvnr.spi.TestKrankenversichertennummerValueProvider;

import java.util.Locale;
import java.util.Map;
import static java.util.Map.entry;
import java.util.Objects;
import java.util.Set;
import java.util.HashSet;
import static java.lang.Integer.parseInt;
import java.util.ServiceLoader;

/**
 * Container for a the 10-digit invariant part of the German Krankenversichertennummer (health insuree number).
 *
 * <p>The implementation validates the 10-digit number following the rules of the
 * <a href='https://en.wikipedia.org/wiki/Luhn_algorithm'>Luhn algorithm</a>.</p>
 *
 * <p>The implementation supports quality assurance health insuree numbers for
 * <a href='https://en.wikipedia.org/wiki/General_Data_Protection_Regulation'>GDPR</a> friendly testing.
 * These numbers are invalid but will pass the Luhn check.
 * By default these numbers are in a range from {@code Q000000000} til {@code Q000000099}.</p>
 * <p>Use the service-provider interface (SPI) of {@link TestKrankenversichertennummerValueProvider} for your own test Krankenversichertennummer values.</p>
 *
 * @see <a href='https://de.wikipedia.org/wiki/Krankenversichertennummer'>Krankenversichertennummer at Wikipedia</a>
 * @see #isTestNumber()
 * @see TestKrankenversichertennummerValueProvider
 */
public final class Krankenversichertennummer {

    private final String number;

    /**
     * Creates a new instance.
     * @param healthInsureeNumber 10-digit invariant part of the German Krankenversichertennummer
     * @throws SyntaxException if and only if the given health insuree number value isn't syntactical correct.
     * @throws ChecksumException if and only if the given health insuree number value has a checksum failure.
     * @throws NullPointerException if and only if {@code number} is {@code null}
     * @see #isTestNumber()
     */
    public Krankenversichertennummer(final String healthInsureeNumber) {
        Objects.requireNonNull(healthInsureeNumber, "Krankenversichertennummer must be provided");
        this.number = normalize(healthInsureeNumber);
        if (!this.number.matches("^([A-Z])[0-9]{9}")) {
            throw new SyntaxException("invalid structure");
        }
        if (!isTestNumber()) {
            checkLuhn(this.number);
        }
    }
    
    private static String normalize(final String healthInsureeNumber) {
        return healthInsureeNumber.toUpperCase(Locale.ENGLISH);
    }

    private void checkLuhn(final String healthInsureeNumber) {
        assert healthInsureeNumber != null;
        assert !healthInsureeNumber.isEmpty();

        final var replacedLetterInsuranceNumber = replaceLetter(healthInsureeNumber);
        final var lastIndex = replacedLetterInsuranceNumber.length() - 1;

        int sum = 0;
        for (int i = 0; i < lastIndex; i++) {
            final var digitChar = "" + replacedLetterInsuranceNumber.charAt(i);
            var digit = parseInt(digitChar);
            if (i % 2 == 1) {
                digit *= 2;
            }
            sum += digit > 9 ? digit - 9 : digit;
        }

        if (sum % 10 != extractChecksum(replacedLetterInsuranceNumber)) {
            throw new ChecksumException("Luhn check failed");
        }
    }

    private int extractChecksum(final String healthInsureeNumber) {
        assert healthInsureeNumber != null;
        assert !healthInsureeNumber.isEmpty();

        return parseInt("" + healthInsureeNumber.charAt(healthInsureeNumber.length() - 1));
    }

    private String replaceLetter(final String healthInsureeNumber) {
        assert healthInsureeNumber != null;
        assert !healthInsureeNumber.isEmpty();

        final var letter = "" + healthInsureeNumber.charAt(0);
        final var letterReplacementNumber = LETTERS_REPLACEMENT_NUMBERS.get(letter);
        return number.replace(letter, letterReplacementNumber);
    }

    /**
     * Check for a quality assurance Krankenversichertennummer value.
     * @return {@code true} if and only if the number is a supported <em>invalid</em> Krankenversichertennummer. {@code false} otherwise.
     */
    public boolean isTestNumber() {
        assert this.number != null;
        if (TEST_NUMBER_SET.contains(this.number)) {
            try {
                checkLuhn(this.number);
            } catch (final ChecksumException ex) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns a {@code String} representation of the German Krankenversichertennummer
     * @return the 10-digit invariant part of the German Krankenversichertennummer as string
     */
    public String value() {
        return this.number;
    }

    private final static Map<String, String> LETTERS_REPLACEMENT_NUMBERS = Map.ofEntries(
            entry("A", "01"),
            entry("B", "02"),
            entry("C", "03"),
            entry("D", "04"),
            entry("E", "05"),
            entry("F", "06"),
            entry("G", "07"),
            entry("H", "08"),
            entry("I", "09"),
            entry("J", "10"),
            entry("K", "11"),
            entry("L", "12"),
            entry("M", "13"),
            entry("N", "14"),
            entry("O", "15"),
            entry("P", "16"),
            entry("Q", "17"),
            entry("R", "18"),
            entry("S", "19"),
            entry("T", "20"),
            entry("U", "21"),
            entry("V", "22"),
            entry("W", "23"),
            entry("X", "24"),
            entry("Y", "25"),
            entry("Z", "26")
    );

    @Override
    public boolean equals(final Object object) {
        if (this == object) { return true; }
        if (!(object instanceof Krankenversichertennummer)) { return false; }
        final var that = (Krankenversichertennummer) object;
        return Objects.equals(this.number, that.number);
    }

    /**
     * The implementation returns the hash code of the underlying string representation of the Krankenversichertennummer value.
     * @return a hash value
     * @see #value()
     */
    @Override
    public int hashCode() {
        return this.number.hashCode();
    }

    /**
     * A loggable string representation.
     * <p><strong>Note:</strong> The value might be <a href='https://en.wikipedia.org/wiki/General_Data_Protection_Regulation'>GDPR</a> relevant.</p>
     * <p>The format of the out may change in future versions without any further information.</p>
     * @return a string representation
     * @see #value()
     */
    @Override
    public String toString() {
        return "Krankenversichertennummer{" + "value='" + number + '\'' + '}';
    }

    private static final Set<String> DEFAULT_TEST_NUMBER_SET = Set.of(
            "Q000000000", "Q000000001", "Q000000002", "Q000000003", "Q000000004", "Q000000006", "Q000000007", "Q000000008", "Q000000009",
            "Q000000010", "Q000000011", "Q000000012", "Q000000013", "Q000000014", "Q000000015", "Q000000016", "Q000000017", "Q000000019",
            "Q000000021", "Q000000022", "Q000000023", "Q000000024", "Q000000025", "Q000000026", "Q000000027", "Q000000028", "Q000000029",
            "Q000000030", "Q000000031", "Q000000033", "Q000000034", "Q000000035", "Q000000036", "Q000000037", "Q000000038", "Q000000039",
            "Q000000040", "Q000000041", "Q000000042", "Q000000043", "Q000000045", "Q000000046", "Q000000047", "Q000000048", "Q000000049",
            "Q000000050", "Q000000051", "Q000000052", "Q000000053", "Q000000054", "Q000000055", "Q000000056", "Q000000058", "Q000000059",
            "Q000000060", "Q000000061", "Q000000062", "Q000000063", "Q000000064", "Q000000065", "Q000000066", "Q000000067", "Q000000068",
            "Q000000070", "Q000000072", "Q000000073", "Q000000074", "Q000000075", "Q000000076", "Q000000077", "Q000000078", "Q000000079",
            "Q000000080", "Q000000081", "Q000000082", "Q000000084", "Q000000085", "Q000000086", "Q000000087", "Q000000088", "Q000000089",
            "Q000000090", "Q000000091", "Q000000092", "Q000000093", "Q000000094", "Q000000096", "Q000000097", "Q000000098", "Q000000099"
    );

    static {
        final var qualityAssuranceValues = new HashSet<String>();
        ServiceLoader.load(TestKrankenversichertennummerValueProvider.class).forEach(provider -> 
            provider.values().stream()
                    .filter(number -> number != null)
                    .map(number -> normalize(number))
                    .forEach(number -> qualityAssuranceValues.add(number))
        );
        if (qualityAssuranceValues.isEmpty()) {
            qualityAssuranceValues.addAll(DEFAULT_TEST_NUMBER_SET);
        }

        TEST_NUMBER_SET = Set.copyOf(qualityAssuranceValues);
    }

    private final static Set<String> TEST_NUMBER_SET;
}
