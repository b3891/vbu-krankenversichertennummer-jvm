/* VBU Krankenversichertennummer - Copyright (C) 2022 BKK VBU
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * https://www.gnu.org/licenses/lgpl-3.0.en.html
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package tech.vbu.kvnr.spi;

import tech.vbu.kvnr.Krankenversichertennummer;

import java.util.Set;

/**
 * Service-provider for quality assurance Krankenversichertennummer values.
 * Such values <a href='https://tools.ietf.org/html/rfc8174'>SHOULD NOT</a> be valid
 * values in order to prevent <a href='https://en.wikipedia.org/wiki/General_Data_Protection_Regulation'>GDPR</a> problems.
 */
public abstract class TestKrankenversichertennummerValueProvider {

    public TestKrankenversichertennummerValueProvider() {}

    /**
     * Implementations should return only invalid Krankenversichertennummer values.
     * This means the values doesn't pass the <a href='https://en.wikipedia.org/wiki/Luhn_algorithm'>Luhn algorithm</a> check.
     * @return a set of invalid Krankenversichertennummer values
     * @see Krankenversichertennummer#isTestNumber()
     * @see Krankenversichertennummer#value()
     */
    public abstract Set<String> values();
}
