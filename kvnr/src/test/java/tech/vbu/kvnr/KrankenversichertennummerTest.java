/* VBU Krankenversichertennummer - Copyright (C) 2022 BKK VBU
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * https://www.gnu.org/licenses/lgpl-3.0.en.html
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package tech.vbu.kvnr;


import static org.junit.Assert.assertThrows;

import static com.google.common.truth.Truth.assertThat;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class KrankenversichertennummerTest {

    @Test
    @DisplayName("check valid Krankenversichertennummer")
    public void validNumber() {
        new Krankenversichertennummer("Z639669360");
    }

    @Test
    @DisplayName("check valid Krankenversichertennummer with lower case leading character")
    public void validNumberLowerCase() {
        // Postel's Law
        // Given
        final var healthInsureeNumber = new Krankenversichertennummer("z639669360");

        // When
        final var value = healthInsureeNumber.value();

        // Then
        assertThat(value).isEqualTo("Z639669360");
    }

    @Test
    @DisplayName("check structural invalid Krankenversichertennummer")
    public void structuralInvalid() {
        // When
        final var ex = assertThrows(SyntaxException.class, () -> new Krankenversichertennummer("1639669360"));

        // Then
        assertThat(ex.getMessage()).isEqualTo("invalid structure");
    }

    @Test
    @DisplayName("check Luhn check fail")
    public void luhnCheckFailed() {
        // When
        final var ex = assertThrows(ChecksumException.class, () -> new Krankenversichertennummer("Z139669360"));

        // Then
        assertThat(ex.getMessage()).isEqualTo("Luhn check failed");
    }

    @Test
    @DisplayName("create with null")
    public void createWithNull() {
        assertThrows(NullPointerException.class, () -> new Krankenversichertennummer(null));
    }

    @Test
    @DisplayName("equal with same instance")
    public void equalWithSameInstance() {
        // Given
        final var healthInsureeNumber = new Krankenversichertennummer("Z639669360");

        // When
        final var result = healthInsureeNumber.equals(healthInsureeNumber);

        // Then
        assertThat(result).isTrue();
    }

    @Test
    @DisplayName("equal with different instance")
    public void equalWithDifferentInstance() {
        // Given
        final var healthInsureeNumberOdd = new Krankenversichertennummer("Z639669360");
        final var healthInsureeNumberEven = new Krankenversichertennummer("Z639669360");

        // When
        final var result = healthInsureeNumberOdd.equals(healthInsureeNumberEven);

        // Then
        assertThat(result).isTrue();
    }

    @Test
    @DisplayName("not equal with null")
    public void equalWithNull() {
        // Given
        final var healthInsureeNumber = new Krankenversichertennummer("Z639669360");

        // When
        final var result = healthInsureeNumber.equals(null);

        // Then
        assertThat(result).isFalse();
    }

    @Test
    @DisplayName("not equal with different insuree numbers")
    public void equalWithDifferentInsuranceNumber() {
        // Given
        final var healthInsureeNumber = new Krankenversichertennummer("Z639669360");

        // When
        final var result = healthInsureeNumber.equals(new Krankenversichertennummer("Q000000090"));

        // Then
        assertThat(result).isFalse();
    }

    @Test
    @DisplayName("not equal because of different typed")
    public void notEqualBecaueDifferentTypes() {
        // Given
        final var healthInsureeNumber = new Krankenversichertennummer("Z639669360");

        // When
        final var result = healthInsureeNumber.equals("other type");

        // Then
        assertThat(result).isFalse();
    }

    @Test
    @DisplayName("check test insured number")
    public void checkTestInsuranceNumber() {
        // Given
        final var healthInsureeNumber = new Krankenversichertennummer("Q000000000");

        // When
        final var result = healthInsureeNumber.isTestNumber();

        // Then
        assertThat(result).isTrue();
    }

    @Test
    @DisplayName("same hash different instance with equal number")
    public void sameHashDifferentButEqualInstances() {
        // Given
        final var healthInsureeNumber1 = new Krankenversichertennummer("Q000000000");
        final var healthInsureeNumber2 = new Krankenversichertennummer("Q000000000");

        // When
        final var hash1 = healthInsureeNumber1.hashCode();
        final var hash2 = healthInsureeNumber2.hashCode();

        // Then
        assertThat(hash1).isEqualTo(hash2);
    }

    @Test
    @DisplayName("different hash for different insured number")
    public void differentHashCodeForDifferentInsuranceNumber() {
        // Given
        final var healthInsureeNumber1 = new Krankenversichertennummer("Q000000000");
        final var healthInsureeNumber2 = new Krankenversichertennummer("Q000000001");

        // When
        final var hash1 = healthInsureeNumber1.hashCode();
        final var hash2 = healthInsureeNumber2.hashCode();

        // Then
        assertThat(hash1).isNotEqualTo(hash2);
    }

    @Test
    @DisplayName("check toString")
    public void checkToString() {
        // Given
        final var healthInsureeNumber = new Krankenversichertennummer("Q000000000");

        // When
        final var asString = healthInsureeNumber.toString();

        // Then
        assertThat(asString).isEqualTo("Krankenversichertennummer{value='Q000000000'}");
    }
}
