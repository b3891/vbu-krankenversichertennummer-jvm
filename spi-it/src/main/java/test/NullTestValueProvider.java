/* VBU Krankenversichertennummer - Copyright (C) 2022 BKK VBU
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * https://www.gnu.org/licenses/lgpl-3.0.en.html
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package test;

import java.util.HashSet;
import java.util.Set;
import tech.vbu.kvnr.spi.TestKrankenversichertennummerValueProvider;

/**
 *
 * @author saschakohlmann
 */
public class NullTestValueProvider extends TestKrankenversichertennummerValueProvider {

    @Override
    public Set<String> values() {
        final var values = new HashSet<String>();
        values.add(null);
        return values;
    }
    
}
